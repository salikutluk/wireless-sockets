package com.salikutluk.smarthome.sockets.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import com.salikutluk.smarthome.sockets.model.Socket;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class SocketRepositoryTest {

	@Autowired
	private SocketRepository repo;

	@Before
	public void setup() {
		repo.save(new Socket(1, "pulselength", "protocol", "code", true));
	}

	@Test
	public void testFind() throws NotFoundException {
		Socket socket = repo.findById(1L).orElseThrow(NotFoundException::new);
		assertThat(socket.isOn()).isTrue();
	}

	@Test
	public void testUpdate() throws NotFoundException {
		Socket socket = repo.save(new Socket(1, "pulselength", "protocol", "code", false));
		assertThat(socket.isOn()).isFalse();
		assertThat(repo.findAll().size()).isEqualTo(1);
	}

}
