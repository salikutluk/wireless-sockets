package com.salikutluk.smarthome.sockets.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CommandExecuterTest {

	private static final String TEST_STRING = "HelloWorld";

	@Test
	public void testCommandExecuterSynch() {
		CommandLineExecuter executer = new CommandLineExecuter();
		String response = executer.execute("echo", TEST_STRING);
		String response1 = executer.execute("echo", TEST_STRING + 123);
		String response2 = executer.execute("echo", TEST_STRING + 456);
		assertEquals(TEST_STRING, response);
		assertEquals(TEST_STRING + 123, response1);
		assertEquals(TEST_STRING + 456, response2);
	}

	@Test
	public void testCommandExecuterAsynch() {
		CommandLineExecuter executer = new CommandLineExecuter();
		String response = executer.execute("echo", TEST_STRING);
		new Thread(() -> {
			String response1 = executer.execute("echo", TEST_STRING + 123);
			assertEquals(TEST_STRING + 123, response1);
		}).start();
		new Thread(() -> {
			String response2 = executer.execute("echo", TEST_STRING + 456);
			assertEquals(TEST_STRING + 456, response2);
		}).start();
		assertEquals(TEST_STRING, response);
	}

}
