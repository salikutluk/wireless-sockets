package com.salikutluk.smarthome.sockets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.salikutluk.smarthome.sockets.command.CommandExecuter;
import com.salikutluk.smarthome.sockets.command.TestCommandLineExecuter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WirelessSocketsApplicationTests {

	@TestConfiguration
	public static class WirelessSocketsApplicationTestsConfig {
		@Bean
		public CommandExecuter executer() {
			return new TestCommandLineExecuter();
		}
	}

	@Test
	public void contextLoads() {}

}
