package com.salikutluk.smarthome.sockets.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import com.salikutluk.smarthome.sockets.command.CommandExecuter;
import com.salikutluk.smarthome.sockets.command.TestCommandLineExecuter;
import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.repository.SocketRepository;
import com.salikutluk.smarthome.sockets.service.impl.SocketServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class SocketServiceImplTest {

	@TestConfiguration
	public static class SocketServiceImplTestConfig {
		@Bean
		public CommandExecuter executer() {
			return new TestCommandLineExecuter();
		}

		@Bean
		public SocketService service() {
			return new SocketServiceImpl();
		}
	}

	@Autowired
	private SocketService service;
	@Autowired
	private SocketRepository repo;

	@Test
	public void testServiceExecutesAndSaves() throws NotFoundException {
		Socket socket = new Socket(1, "pulselength", "protocol", "code", true);
		service.sendRequest(socket);
		Socket foundSocket = repo.findById(1L).orElseThrow(NotFoundException::new);
		assertThat(foundSocket.isOn()).isTrue();
	}

}
