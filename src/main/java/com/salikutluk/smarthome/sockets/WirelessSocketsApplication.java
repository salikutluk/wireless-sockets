package com.salikutluk.smarthome.sockets;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.repository.SocketRepository;

@SpringBootApplication
public class WirelessSocketsApplication implements ApplicationRunner {

	@Autowired
	private SocketRepository repo;

	public static void main(String[] args) {
		SpringApplication.run(WirelessSocketsApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		if (repo.findAll().size() < 3) {
			List<Socket> sockets = Arrays.asList(new Socket(1, "", "", "", false), new Socket(2, "", "", "", false), new Socket(3, "", "", "", false));
			repo.saveAll(sockets);
		}
	}

}
