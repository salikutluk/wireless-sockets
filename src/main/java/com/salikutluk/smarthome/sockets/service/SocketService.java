package com.salikutluk.smarthome.sockets.service;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import com.salikutluk.smarthome.sockets.model.Socket;

@Service
public interface SocketService {

	public Socket sendRequest(Socket socket);

	public Socket getStatus(long id) throws NotFoundException;
}
