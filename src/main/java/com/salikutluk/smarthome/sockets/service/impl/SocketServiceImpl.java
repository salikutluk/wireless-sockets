package com.salikutluk.smarthome.sockets.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import com.salikutluk.smarthome.sockets.command.CommandExecuter;
import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.repository.SocketRepository;
import com.salikutluk.smarthome.sockets.service.SocketService;

@Service
public class SocketServiceImpl implements SocketService {

	@Autowired
	private CommandExecuter executer;
	@Autowired
	private SocketRepository repo;
	@Value("${send.script.path}")
	private String pathToScript;

	@Override
	public Socket sendRequest(Socket socket) {
		executer.execute("python3", pathToScript, "-p", socket.getPulselength(), "-t", socket.getProtocol(), socket.getCode());
		return repo.save(socket);
	}

	@Override
	public Socket getStatus(long id) throws NotFoundException {
		return repo.findById(id).orElseThrow(NotFoundException::new);
	}

}
