package com.salikutluk.smarthome.sockets.command;

public interface CommandExecuter {

	public String execute(String... args);
}
