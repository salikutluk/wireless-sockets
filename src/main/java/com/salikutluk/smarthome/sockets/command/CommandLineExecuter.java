package com.salikutluk.smarthome.sockets.command;

import org.springframework.stereotype.Component;

@Component
public class CommandLineExecuter implements CommandExecuter {

	@Override
	public String execute(String... args) {
		com.salikutluk.smarthome.commandline.CommandLineExecuter executer = new com.salikutluk.smarthome.commandline.CommandLineExecuter();
		return executer.executeCommand(args);
	}

}
