package com.salikutluk.smarthome.sockets.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Socket {

	@Id
	private long id;
	private String pulselength, protocol, code;
	private boolean isOn;

	public Socket() {}

	public Socket(long id, String pulselength, String protocol, String code, boolean isOn) {
		this.id = id;
		this.pulselength = pulselength;
		this.protocol = protocol;
		this.code = code;
		this.isOn = isOn;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPulselength() {
		return pulselength;
	}

	public void setPulselength(String pulselength) {
		this.pulselength = pulselength;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setIsOn(boolean isOn) {
		this.isOn = isOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Socket other = (Socket) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
