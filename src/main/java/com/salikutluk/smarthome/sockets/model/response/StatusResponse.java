package com.salikutluk.smarthome.sockets.model.response;

public class StatusResponse {
	private boolean statusPattern = false;

	public StatusResponse() {}

	public StatusResponse(boolean statusPattern) {
		this.statusPattern = statusPattern;
	}

	public boolean isStatusPattern() {
		return statusPattern;
	}

	public void setStatusPattern(boolean statusPattern) {
		this.statusPattern = statusPattern;
	}

}
