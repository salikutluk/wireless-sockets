package com.salikutluk.smarthome.sockets.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salikutluk.smarthome.sockets.model.Socket;

public interface SocketRepository extends JpaRepository<Socket, Long> {

}
