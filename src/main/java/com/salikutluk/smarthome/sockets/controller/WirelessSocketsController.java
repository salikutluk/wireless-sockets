package com.salikutluk.smarthome.sockets.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.model.response.StatusResponse;
import com.salikutluk.smarthome.sockets.service.SocketService;

@RestController
public class WirelessSocketsController {

	@Autowired
	private SocketService service;

	@GetMapping(path = "/status/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<StatusResponse> getResponse(@PathVariable("id") int id) throws NotFoundException {
		return ResponseEntity.ok(new StatusResponse(service.getStatus(id).isOn()));
	}

	@PostMapping(path = "/turnOn", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<HttpStatus> turnOn(@RequestBody Socket socket) throws IOException, InterruptedException {
		return sendRequest(socket);
	}

	@PostMapping(path = "/turnOff", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<HttpStatus> turnOff(@RequestBody Socket socket) throws IOException, InterruptedException {
		return sendRequest(socket);
	}

	private ResponseEntity<HttpStatus> sendRequest(Socket socket) throws IOException, InterruptedException {
		service.sendRequest(socket);
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
}
